#ifndef INCLUDE_TFRCLUSTER_H
#define INCLUDE_TFRCLUSTER_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of a cluster
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//custom libraries
#include "TFRLayer.h"
#include "TFRMCHit.h"

using namespace std;

class TFRCluster : public TObject {

 public:
  
  /*! Empty constructor */
  TFRCluster(){ };

  /*! Standard constructor, with cluster positions */
  TFRCluster(TFRLayer _layer, double _x_min, double _x_max, double _y_min, double _y_max, double _width,
	     double _z = -999999., TFRMCHits _hits = {}) :
  layer(_layer), x_min(_x_min), x_max(_x_max), y_min(_y_min), y_max(_y_max), width(_width), z(_z), hits(_hits) {
    channelID = "9999999999";
  }

  /*! Destructor */
  virtual ~TFRCluster( ){ };

  /*! Returns the detector layer */
  inline TFRLayer GetLayer() const {return layer;};

  /*! Returns the MC hits from which the cluster is created */
  inline TFRMCHits GetHits() const {return hits;};

  /*! Add a MC hit contribution to the cluster */
  inline void AddHit(TFRMCHit *hit){ hits.Add(hit); };
  
  /*! Returns the channelID of the cluster */
  std::string GetChannelID() const {return channelID;};

  /*! Returns the mean y position of the cluster */
  inline double GetY() const {return (this->y_min + this->y_max)/2.;};
  
  /*! Returns the mean x position of the cluster */
  inline double GetX() const {return (this->x_min + this->x_max)/2.;};
  
  /*! Returns the width of the cluster (useful for strips) */
  inline double GetWidth() const {return width;};

  /*! Returns the x and y position of the cluster: different for pixel and strips! */
  TVectorD GetMeasure();
  
  /*! Returns the z position of the cluster */
  inline double GetZ() const {return z;};

  /*! Returns the measurment covariance */
  TMatrixD GetCovariance();

  /*! Returns the error on the x position of the cluster: at first order is the X segmentation of the sensors */
  inline double GetXErr() const {return fabs(this->x_max - this->x_min)/sqrt(12.);};
  
  /*! Equal comparison checking the channel ID, to handlw the TObjArray::FindOBject feature */
  //Bool_t IsEqual(const TFRCluster *cluster) const {return (this->channelID == cluster->channelID);};

 protected:
    
 private:
  
  /*! Layer of the cluster */
  TFRLayer layer;
  
  /*! ChannelID of the cluster */
  std::string channelID;
  
  /*! X coordinates of the cluster */
  double x_min, x_max;
  
  /* Y coordinates of the cluster */
  double y_min, y_max;

  /*! width of the cluster (important for stips)*/
  double width;
  
  /*! z position of the cluster */
  double z;
  
  /*! MC hits from which the cluster is created */
  TFRMCHits hits;

  ClassDef(TFRCluster, 1)
    
} ;

/*! Array of clusters */
typedef TObjArray TFRClusters;

#endif // INCLUDE_TFRCLUSTER_H
